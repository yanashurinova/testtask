FROM python

WORKDIR /code

COPY . .

RUN pip install databases
RUN pip install python-dotenv
RUN pip install asyncpg
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
RUN pip install fastapi uvicorn
RUN pip install pydantic[email]
RUN pip install python-multipart



CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
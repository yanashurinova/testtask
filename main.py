from app.models.database import database
from fastapi import FastAPI
from app.routers import users

app = FastAPI()

# app.include_router(signup.router)
# app.include_router(auth.router)
app.include_router(users.router)


@app.on_event("startup")
async def startup():
    # когда приложение запускается устанавливаем соединение с БД
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    # когда приложение останавливается разрываем соединение с БД
    await database.disconnect()


@app.get("/")
async def read_root():
    return {}




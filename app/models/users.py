import sqlalchemy

from app.models.database import metadata

# metadata = sqlalchemy.MetaData()

users_table = sqlalchemy.Table(
    "users",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True, index=True),
    sqlalchemy.Column("email", sqlalchemy.String(40), unique=True, nullable=False),
    sqlalchemy.Column("name", sqlalchemy.String(100), nullable=False),
    sqlalchemy.Column("hashed_password", sqlalchemy.String(), nullable=False),
    sqlalchemy.Column("salary", sqlalchemy.Integer, nullable=False),
    sqlalchemy.Column("promotion_date", sqlalchemy.DateTime()),
    sqlalchemy.Column(
        "is_active",
        sqlalchemy.Boolean(),
        server_default=sqlalchemy.sql.expression.true(),
        nullable=False,
    ),

)








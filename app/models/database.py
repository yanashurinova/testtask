import databases
import sqlalchemy
from config import DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS

from os import environ
import databases
from config import DB_HOST_TEST, DB_PORT_TEST, DB_NAME_TEST, DB_USER_TEST, DB_PASS_TEST

metadata = sqlalchemy.MetaData()

SQLALCHEMY_DATABASE_URL = (
    f"postgresql://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
)

database = databases.Database(SQLALCHEMY_DATABASE_URL)
# TESTING = environ.get("TESTING")
# if TESTING == 'True':
#     TEST_SQLALCHEMY_DATABASE_URL = (
#         f"postgresql://{DB_USER_TEST}:{DB_PASS_TEST}@{DB_HOST_TEST}:{DB_PORT_TEST}/{DB_NAME_TEST}"
#     )
#     database = databases.Database(TEST_SQLALCHEMY_DATABASE_URL)
# else:
#     SQLALCHEMY_DATABASE_URL = (
#         f"postgresql://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
#     )
#     database = databases.Database(SQLALCHEMY_DATABASE_URL)

import os
from main import app
from fastapi.testclient import TestClient
import pytest
from alembic import command
from alembic.config import Config
from app.models import database
from sqlalchemy_utils import create_database, drop_database

client = TestClient(app)


@pytest.fixture(scope="module")
def temp_db():
    create_database(database.TEST_SQLALCHEMY_DATABASE_URL)  # Создаем БД
    base_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "test_task")
    alembic_cfg = Config(os.path.join(base_dir, "alembic.ini"))  # Загружаем конфигурацию alembic
    command.upgrade(alembic_cfg, "head")  # выполняем миграции

    try:
        yield database.TEST_SQLALCHEMY_DATABASE_URL
    finally:
        drop_database(database.TEST_SQLALCHEMY_DATABASE_URL)  # удаляем БД


def test_sign_up(temp_db):
    request_data = {
        "email": "ivan@gmail.com",
        "name": "Ivan Ivanov",
        "password": "rainbow",
        "salary": 100,
        "promotion_date": "2023-06-30T18:46:41.397"
    }
    with TestClient(app) as client:
        response = client.post("/sign-up", json=request_data)
    assert response.status_code == 200
    assert response.json()["id"] == 1
    assert response.json()["email"] == "ivan@gmail.com"
    assert response.json()["name"] == "Ivan Ivanov"
    assert response.json()["salary"] == 100
    assert response.json()["promotion_date"] == "2023-06-30T18:46:41.397"
    assert response.json()["token"]["expires"] is not None
    assert response.json()["token"]["access_token"] is not None
